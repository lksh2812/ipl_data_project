import csv
import matplotlib.pyplot as plt
from extract_data_lib import dict_to_list
# plt.style.use("fivethirtyeight")
# plt.style.use("ggplot")

def cal_data(file_name):
    data_dict = {}
    season_dict = set() 
    with open(file_name) as matches_file:
        matches_reader = csv.DictReader(matches_file)
        for match in matches_reader:
            if match['winner'] not in data_dict.keys():
                data_dict.update({match['winner']: {match['season']:1}})  
            else:
                if match['season'] not in data_dict[match['winner']].keys():
                    data_dict[match['winner']].update({match['season']:1})   
                else:
                    n = data_dict[match['winner']]
                    n[match['season']]+=1
                    season_dict.update({match['season']})
        
    season_list = list(season_dict)
    return data_dict, season_list


def refine_dict(data_dict, season_list):
    for value in data_dict.values():
        for year in season_list:
            if year not in value.keys():
                value.update({year:0})


def cal_bottom(n):
    if n < 1:
        return 0
    else:
        base = data_list[0][2]

    for j in range(1,n):
        base = list([x + y for x, y in zip(data_list[j][2], base)])
    return base


if __name__ == "__main__":
    data_dict, season_list = cal_data("ipl/matches.csv")
    season_list.sort()
    refine_dict(data_dict, season_list)
    data_list = dict_to_list(data_dict)
    
    for i in range(len(data_list)):
        plt.bar(data_list[i][1],data_list[i][2],bottom=cal_bottom(i), label=data_list[i][0])

    plt.legend(loc='upper right', ncol=5, fontsize = 'x-small')
    plt.title("Matches won by each Team for sach season")
    plt.ylim((0,90))
    plt.show()
