# import sys
from extract_data_lib import extract_season_matchids, season_matchid, dict_to_list
import matplotlib.pyplot as plt
plt.style.use("fivethirtyeight")

def batsmen_data(season_match_data):
    batsmen_sixes = {}
    death_over = ['18','19','20']
    for match in season_match_data:
        if match['is_super_over']=='0' and match['over'] in death_over and match['batsman_runs']=='6':
            if match['batsman'] not in batsmen_sixes.keys():
                batsmen_sixes.update({match['batsman']: 1})
            else:
                batsmen_sixes[match['batsman']] += 1
    return batsmen_sixes


def cal_data():
    bat_dict = {}
    for season in range(2008,2018):
        match_ids = extract_season_matchids(str(season),'ipl/matches.csv')
        season_match_data = season_matchid(match_ids,'ipl/deliveries.csv')
    
        batsmen_sixes = batsmen_data(season_match_data)
        batsmen = sorted(batsmen_sixes, key=batsmen_sixes.get)[-3:]
        
        for batsman in batsmen:
            if batsman not in bat_dict.keys():
                bat_dict.update({batsman:{season:batsmen_sixes[batsman]}}) 
            else:
                bat_dict[batsman].update({season:batsmen_sixes[batsman]})
        
    return bat_dict


if __name__ == "__main__":
    bat_dict = cal_data()
    data_list = dict_to_list(bat_dict)
    print(data_list)

    for i in range(len(data_list)):
        plt.plot(data_list[i][1],data_list[i][2], '-o', label=data_list[i][0])

    plt.legend(loc='upper right', ncol=5, fontsize = 'x-small')
    plt.ylim((0,18))
    plt.show()
    