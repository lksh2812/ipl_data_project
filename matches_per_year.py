import csv
from collections import Counter
import matplotlib.pyplot as plt
plt.style.use("fivethirtyeight")

def calculate(file_name):
    with open(file_name) as matches_file:
        matches_reader = csv.DictReader(matches_file)
        matches_counter = Counter()
        for match in matches_reader:
            matches_counter.update([match['season']])
        return matches_counter

def plot(result):
    seasons = []
    matches = []

    for key, value in sorted(result.items()):
        seasons.append(key)
        matches.append(value)

    plt.bar(seasons,matches)
    plt.title("Matches per Year")
    plt.xlabel("Seasons")
    plt.ylabel("No. of Matches")
    plt.show()

def execute():
    plot(calculate("ipl/matches.csv"))

execute()
