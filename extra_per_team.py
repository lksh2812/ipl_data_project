from extract_data_lib import extract_season_matchids, season_matchid
import matplotlib.pyplot as plt
plt.style.use("fivethirtyeight")


def cal_team_extra_runs(season_match_data):
    team_runs = {}
    for match in season_match_data:
        if match['extra_runs']!= '0':
            if match['bowling_team'] not in team_runs.keys():
                team_runs.update({match['bowling_team']:int(match['extra_runs'])})
            else:
                team_runs[match['bowling_team']] += int(match['extra_runs'])
    return team_runs


def plot(team_runs):
    teams = []
    runs = []
    for key, value in team_runs.items():
        teams.append(key)
        runs.append(value)

    plt.barh(teams,runs)
    plt.title("Extra runs per Team for 2016")
    plt.xlabel("Extras consceded")
    plt.show()


if __name__ == "__main__":
    match_ids = extract_season_matchids('2016','ipl/matches.csv')
    season_match_data = season_matchid(match_ids,'ipl/deliveries.csv')
    team_runs = cal_team_extra_runs(season_match_data)
    plot(team_runs)
