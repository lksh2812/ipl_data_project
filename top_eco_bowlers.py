from extract_data_lib import extract_season_matchids, season_matchid
import matplotlib.pyplot as plt
plt.style.use("fivethirtyeight")


def bowler_data(season_match_data):
    bowler_runs = {}
    bowler_balls = {}
    for match in season_match_data:
        if match['is_super_over']=='0':
            run = int(match['wide_runs'])+int(match['noball_runs'])+int(match['batsman_runs'])
            
            if match['wide_runs']=='0' and match['noball_runs']=='0':
                if match['bowler'] not in bowler_balls.keys():
                    bowler_balls.update({match['bowler']:1})
                else:
                    bowler_balls[match['bowler']] += 1

            if match['bowler'] not in bowler_runs.keys():
                bowler_runs.update({match['bowler']:run})
            else:
                bowler_runs[match['bowler']] += run
    return bowler_runs,bowler_balls


def calculate_economy(bowler_balls,bowler_runs):
    bowler_eco = {}
    for bowler, ball in bowler_balls.items():
        eco = (int(bowler_runs[bowler])/int(ball))*6
        bowler_eco.update({bowler:eco})
    return bowler_eco


if __name__ == "__main__":
    match_ids = extract_season_matchids('2015','ipl/matches.csv')
    season_match_data = season_matchid(match_ids,'ipl/deliveries.csv')

    bowler_runs, bowler_balls = bowler_data(season_match_data)
    bowler_eco = calculate_economy(bowler_balls,bowler_runs)
    bowlers = sorted(bowler_eco, key=bowler_eco.get)[0:10]

    economies = []
    for bowler in bowlers:
        economies.append(bowler_eco[bowler])

    plt.barh(bowlers,economies)
    plt.xlabel("Economy")
    plt.title("Top economical bowlers for season 2015")
    plt.show()
    