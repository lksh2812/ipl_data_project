import csv
def extract_season_matchids(season, file):
    match_data = []
    with open(file) as match_file:
        match_reader = csv.DictReader(match_file)
        match_data = list(filter(lambda match:match['season']==season,match_reader))
        match_ids = list(map(lambda match:match['id'],match_data))
    return match_ids


def season_matchid(match_ids,file):
    ball_data = []
    with open(file) as deliveries_file:
        ball_reader = csv.DictReader(deliveries_file)
        ball_data = list(filter(lambda match:match['match_id'] in match_ids,ball_reader))
    return ball_data

def dict_to_list(data_dict):
    data_list = []
    for name,sub_dict in data_dict.items():
        if name =="":
            name = "No result"
        seasons = []
        counter = []
        for year, count in sorted(sub_dict.items()):
            seasons.append(year)
            counter.append(count)
        data_list.append([name,seasons,counter])
    return data_list


# def dict_to_list(batsmen_sixes):
#     batsmen = sorted(batsmen_sixes, key=batsmen_sixes.get)[-3:]
#     sixes = []
#     for batsman in batsmen:
#         sixes.append(batsmen_sixes[batsman])
#     return batsmen, sixes

# try:
#     season = input("Enter a season between[2008-2017]")
# except ValueError as e:
#     print("enter a number ",e)

# if int(season) not in range(2008,2018):
#     print("Data not avavilable for this season")
#     sys.exit()

# print(season)
