In this data project we are transforming raw data from IPL into charts that will convey some meaning / analysis. To work on this project we need two csv files matches.csv and deliveries.csv

Download both csv files from https://www.kaggle.com/manasgarg/ipl

To plot charts in this project 'matplotlib' is used

Following graphs have been generated:-

1. Plotted a bar chart of the number of matches played per year of all the years in IPL.
2. Plotted a stacked bar chart of matches won of all teams over all the years of IPL.
3. Plotted a bar chart for the year 2016 extra runs conceded per team.
4. Plotted a bar chart For the year 2015 top economical bowlers.
5. Plotted a bar chart for top 3 batsmen to hit sixes in death overs, over all the years of IPL.         Hence showing the consistency of some players to hit sixes in death overs.

